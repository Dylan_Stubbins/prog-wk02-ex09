﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk02_ex09
{
    class Program
    {
        static void Main(string[] args)
        {
            var again = true;

            do
            {
                Console.WriteLine("Please choose a number");

                var counter = int.Parse(Console.ReadLine());
                var i = 0;

                if (again)
                {
                    for (i = 0; i < counter; i++)
                    {
                        var a = i + 1;
                        Console.WriteLine($"This is line number {a}");
                    }
                }
                if (i == counter)
                {
                    Console.WriteLine("Would you like to choose another number? Type Yes or No");
                    var answer = Console.ReadLine();

                    if ((answer == "Yes") || (answer == "yes"))
                    {
                        again = true;
                    }
                    else
                    {
                        again = false;
                        Console.WriteLine("Thank you for using this app");
                    }

                }
            } while (again == true);
         }

     }
  }

         